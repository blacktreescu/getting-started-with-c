﻿#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//定义函数
float calculateCharges(float);
float input();

int main() {
	float a, b, c;
	a = input();
	b = input();
	c = input();
	printf("Car     Hours     Charge\n");
    printf("1       %5.1f     %6.2f\n", a, calculateCharges(a));
	printf("2       %5.1f     %6.2f\n", b, calculateCharges(b));
	printf("3       %5.1f     %6.2f\n", c, calculateCharges(c));
	printf("TOTAL   %5.1f     %6.2f\n", (double)a+b+c, (double)calculateCharges(a)+calculateCharges(b) +calculateCharges(c));
}
//计算费用
float calculateCharges(float x) {
	if (0 < x && x <= 3) {
		return 2;
	}
	else if (3 <= x && x <= 19) {
		return 2 + 0.5 * (x - 3);
	}
	else{
		return 10;
	}
}
//让用户输入的函数
float input() {
	float t;
	printf("请输入时间（不超过24h）：\n");
	scanf("%f",&t);
	if (t>0 && t <= 24) 
	{
		return t;
	}
	else
	{
		printf("输入非法，请重新输入\n");
		return input();//输入非法后进行递归，再次输入。
	}
}