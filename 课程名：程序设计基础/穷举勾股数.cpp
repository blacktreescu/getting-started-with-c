﻿#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int main(void) {
	long int a = 1, b = 1, c = 1, n;
	printf("%s", "你想获得多少以内的勾股数：");
	scanf("%d", &n);
	printf("%s\n", "勾\t股\t弦");
	for (a; a <= n; a++) {//将最短边从1到n循环
		b = a;            //令	b = a;可以避免重复计算
		for (b; b <= n; b++) {
			c = b;        //c边为直角边，比b大
			for (c; c <= n; c++) {
				if (a * a + b * b == c * c) {//判断
					printf("%d\t%d\t%d\t\n", a, b, c);
				}
			}
		}
	}
}