﻿#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
struct card {//初始化结构体 使用位域 
	const char* face;
	unsigned int number : 4;
}cards[52];
void show_card(struct card* p) {//洗牌
	printf("face   suit  face   suit\n");
	for (int n = 0; n < 52; n++) {//两两随机交换
		int a = rand() % 52;
		struct card temp = *(p + n);*(p + n) = *(p + a);*(p + a) = temp;
	}
	for (int n = 0; n < 52; n++) {//打印
		printf("%s %6d  ", (p + n)->face, (p + n)->number);
		if ((n + 1) % 2 == 0)
			puts("");
	}
}
int main() {
	srand(time(NULL));
	const char* face[4] = { "红桃","黑桃","方块","梅花" };
	for (int i = 0; i < 4; i++) {//初始化一副牌
		for (int j = 0; j < 13; j++) {
			cards[i * 13 + j].face = face[i];cards[i * 13 + j].number = j+1;
		}
	}
	show_card(cards);
}