﻿#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<math.h>
int main(void) {
	double pi=0;
	int c = -1;//c来控制正负
	int i = 1;
	for (i; i < 10000000; i++) {
		c *= -1;
		pi += (double)4 / (2 * i - 1)*c;		//计算PI;
		if (fabs(pi - 3.14) < 1e-6) {
			printf("pi=3.14 \t");
			printf("运算次数：%d\n", i);
			break;                              //break结束循环，接着计算下一个精度 （用while循环也行）
		}
	}
	for (i++; i < 10000000; i++) {              //i++是以为i已经在上一个循环上用过了，要加1
		c *= -1;
		pi += (double)4 / (2 * i - 1) * c;
		if (fabs(pi - 3.141) < 1e-6) {
			printf("pi=3.141\t");
			printf("运算次数：%d\n", i);
			break;
		}
	}
	for (i++; i < 10000000; i++) {
		c *= -1;
		pi += (double)4 / (2 * i - 1) * c;
		if (fabs(pi - 3.1415) < 1e-6) {
			printf("pi=3.1415\t");
			printf("运算次数：%d\n", i);
			break;
		}
	}
	for (i++; i < 10000000; i++) {
		c *= -1;
		pi += (double)4 / (2 * i - 1) * c;
		if (fabs(pi - 3.14159) < 1e-6) {
			printf("pi=3.14159\t");
			printf("运算次数：%d\n", i);
			break;
		}
	}
}


