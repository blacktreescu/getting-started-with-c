﻿#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
void FirstClasse(int a[10]);//用户选择头等舱调用函数
void Economy(int [10]);		//用户选择经济舱调用函数
void printftickets(int);	//打印登机牌
int main() {
	int choose = 0;
	int seat[10] = { 0 };
	do {	
		srand(time(NULL));
		//用户选择
		do {
			printf("please type 1 for \"first class\"\n");
			printf("please type 2 for \"economy\"\n");
			scanf("%d", &choose);
			if (choose != 1 && choose != 2)
				printf("***非法输入，请重试***\n");
		} while (choose != 1 && choose != 2);//判断用户输入是否合法
		//分配座位+打印登机牌
		if (choose == 1) {
			FirstClasse(seat);
		}
		if (choose == 2) {
			Economy(seat);
		}
		//询问是否继续
		printf("===是否开始您的选票?===\n");
		printf("     1.是  2.否    \n");
		do {
			scanf("%d", &choose);
			if (choose != 1 && choose != 2)
				printf("***非法输入，请重试***\n");//判断用户输入是否合法
		} while (choose != 1 && choose != 2);
	} while (choose == 1);
}

void FirstClasse(int a[10]) {
	int i;
	if (a[0] * a[2] * a[3] * a[4] * a[1] == 0) { //检测是否有空位
		do {
			i = rand() % 5 ;
		} while (a[i] == 1);
		a[i] = 1;                                //标记座位
		printftickets(i + 1);					 //打印登机牌
	}
	else {
		printf("***座位已满，是否选择换到econimy?***\n");
		printf("        1.是          2.否 \n");
		int choose = 1;
		do {
			scanf("%d", &choose);
			if (choose != 1 && choose != 2)
				printf("***非法输入，请重试***\n");//判断用户输入是否合法
		} while (choose != 1 && choose != 2);
		if (choose == 1) {
			Economy(a);							  //若用户同意，则进入经济舱选票
		}
		else {
			printf("next fight will leves in three hours\n");//用户不同意，通知下一班。。。
		}
	}
}
void Economy(int a[10]) {
	int i;
	if (a[5]*a[6]*a[7]*a[8]*a[9]==0) {
		do {
			i = rand() % 5 + 5;
		} while (a[i] ==1 );
		a[i] = 1;
		printftickets(i+1);
	}
	else {
		printf("next fight will leves in three hours\n");
	}
}
void printftickets(int i) {
	const char* a[2] = { "First classe", "Economy" };//用const指针来输出字符串
	printf("---------------------------------------\n");
	printf("|         你的座位号为：%2d            |\n",i);
	printf("|         类型： %-15s      |\n", a[i / 6]);
	printf("---------------------------------------\n");
	
}