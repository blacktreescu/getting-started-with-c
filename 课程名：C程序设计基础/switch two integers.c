/*switch two integers using a method without the third variable*/
#include <stdio.h>
#include <stdlib.h>
int main(void)
{
    int num1,num2;
    printf("This is a programme that can switch two numbers,\nlet's have a try right here!\n");
    printf("Now plz down two integers and each one ends with key 'Enter':\n");
    scanf("%d %d", &num1 , &num2 );
    num1 = num1 + num2; //To save their relation using a addition
    num2 = num1 - num2; //later num2
    num1 = num1 - num2; //later num1
    /*
    e.g. "1,2" 
    "3->num1" will be their sum;
    so "3-2=1->num2" ,"3-1=2->num1" with each available variable;
    then finished!
    */
    printf("Now switching for a monment...\nThese two integers were changed to be %d and %d !!!\n",num1,num2);
    system("pause");
    return 0;
}