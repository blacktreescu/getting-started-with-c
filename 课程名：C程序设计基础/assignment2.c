#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 200
int main(){
    int i,j,ml,sl,rl;
    char* m,mstr[N]={0},* s,sstr[N]={0},* first,* newstr;
    m=mstr;s=sstr;//给指针分配内存地址，即消除野指针
    printf("mstr=");
    gets(m);
    newstr=m;
    fflush(stdin);
    printf("sstr=");
    gets(s);
    first=strstr(m,s);//char *strstr(const char *haystack, const char *needle) 
    //该函数返回在 haystack 中第一次出现 needle 字符串的位置及之后的字符（是指针），如果未找到则返回 NULL。
    while(first!=NULL){
        ml=strlen(m);
        sl=strlen(s);
        rl=strlen(first);
        for(i=0;i<ml-rl;i++) *(newstr+i)=*(m+i);//前面不变的字符，输入到newstr中
        for(j=ml-rl+sl;j<ml;j++) newstr[j-sl]=*(m+j);//从替换点（包括）开始后面的项开始，置于替换后应该在的位置上去
        /*
        指针的灵活性充分示例！他们都可以理解为数组，数组的指针默认置于第一个（0）位。
        例如：定义ar[n]，等同于*(ar+n)，也就是到内存ar的位置再移动n个单元，检索在哪里的值。注意*是解引用的含义。
        */
        newstr[j-sl]='\0';
        m=newstr;
        first=strstr(m,s);//上述步骤只完成了第一个字串的检索与替换，需要重置参数、依靠while循环，完成剩余字串的相关操作。
    }
    printf("newstr=%s\n",newstr);
    system("pause");
    return 0;
}