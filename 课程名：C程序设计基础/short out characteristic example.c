/*This is an example showing Short out characteristic 短路特性*/
#include<stdio.h>
#include<stdlib.h>

int main(void)
{   
    int i,j,k;
    i=1;j=1;k=2;
    if ((j++||k++)&&i++)//if j++ value True,(j++||k++) value True without operating k++
    {
        printf("%d,%d,%d\n",i,j,k);
    }
    system("pause");
    return 0;
}