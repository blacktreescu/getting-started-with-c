#include <stdio.h>
#include <stdlib.h>

int main(){
    unsigned long num1,num2,ts,tss,result;
    int n;
    printf("Plz enter an octal number: ");
    scanf("%lo",&num1);//157653-->0000...1101111110101011(有效16位) usigned long 64位
    printf("How many digits to the right?:");
    scanf("%d",&n);//3
    ts=num1<<(64-n);
    num2=num1>>n;
    int ds=0;//该数据有多少位
    tss=num1;
    while(tss!=0){
        tss=tss>>1;
        ds++;
    }
    ts=ts>>(ds);//清除除n位外的所有无效值

    result=(ts|num2);
    printf("Enter:  %lo\nAfter:  %lo\nDigits:  %d\n",num1,result,ds);
    system("pause");
    return 0;
}