#include <stdio.h>
#include <math.h>
#include <stdlib.h>

double factor(int n)
{
    if(n==0)return 1.0;
    double sum_f=1;
    for(int i=1;i<=n;i++)
        sum_f=sum_f*i;
    return sum_f;
}

int main(){
    int n=0;
    double sum=0,x;
    scanf("%lf",&x);
    while((fabs(pow(x,n))/factor(n))>=0.00001){
        sum=sum+(fabs(pow(x,n))/factor(n));
        n++;
    }
    printf("%.4f",sum);
    system("pause");
    return 0;
}
