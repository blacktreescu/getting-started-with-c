/*func:switch example*/
#include <stdio.h>
#include<stdlib.h>
int main(){

	char grade = 'D';
	while(1){
		printf("please input your grade class:A\tB\tC\tD\tE\n");
		scanf("%c",&grade);
		switch(grade){
			case 'A':
				printf("excellent !\n");
			case'B':
				printf("do a good job !\n");
			case'C':
				printf("good !\n");//NO BREAK
			case'D':
				printf("come on , I believe you could do it next time !\n");
				break;
			case'E':
				printf("what's wrong with you ? Come on!\n");
				break;
			default:
				printf("input error !!!\n");
		}
		//break;
	}
	getchar();
	//system("pause");
	return 0;
}

