#include <stdio.h>
#include <stdlib.h>
int main(){
    int inverted(int n);
    int m;
    for(int n=1000;n<10000;n++){
        m=inverted(n);
        if(m==4*n) printf("%d",m);
    }
    system("pause");
    return 0;
}
int inverted(int n){
    int inverted_num,multi,sum;
    sum=0;
    for(int i=0;i<4;i++)
	{ 
		inverted_num=n%10;
		n=n/10;
        switch (i)
        {
        case 0:multi=1000;break;
        case 1:multi=100;break;
        case 2:multi=10;break;
        case 3:multi=1;break;
        }
        sum=sum+inverted_num*multi;
	}
    return sum;
}