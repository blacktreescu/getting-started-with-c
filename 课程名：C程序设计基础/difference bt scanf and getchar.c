/*test for difference between scanf and getchar*/
#include<stdio.h>
#include<stdlib.h>
int main(){
    char c1,c2,c3,c4,c5,c6;
    //input:12\n34567
    scanf("%c%c%c",&c1,&c2,&c3);
    c5=getchar();c6=getchar();
    //output
    printf("%c",c3); 
    putchar(c1);putchar(c2);
    printf("%c%c\n",c5,c6);
    system("pause");
    return 0;
}